build_imagem:
	docker build --tag tempo-agora/front:latest .

container_up:
	docker-compose up -d

deploy_local:
	ng build
	make build_imagem
	make container_up
