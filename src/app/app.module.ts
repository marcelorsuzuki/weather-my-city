import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { PesquisarCidadeComponent } from './tempo/pesquisar.component';
import { InformacaoCidade } from './tempo/info-cidade.component';
import { WeatherService } from './tempo/get-information.service';


@NgModule({
  declarations: [
    AppComponent,
    PesquisarCidadeComponent,
    InformacaoCidade
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule
  ],
  providers: [WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
