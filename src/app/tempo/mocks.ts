export const RESPONSE_CITY = {
  "coord": {
      "lon": -84.23,
      "lat": 9.94
  },
  "weather": [
      {
          "id": 801,
          "main": "Clouds",
          "description": "few clouds",
          "icon": "02d"
      }
  ],
  "base": "stations",
  "main": {
      "temp": 300.14,
      "pressure": 1016,
      "humidity": 51,
      "temp_min": 299.15,
      "temp_max": 301.15
  },
  "visibility": 10000,
  "wind": {
      "speed": 7.2,
      "deg": 60
  },
  "clouds": {
      "all": 20
  },
  "dt": 1530460800,
  "sys": {
      "type": 1,
      "id": 4220,
      "message": 0.0805,
      "country": "CR",
      "sunrise": 1530444008,
      "sunset": 1530489706
  },
  "id": 3621849,
  "name": "Brasil",
  "cod": 200
}
