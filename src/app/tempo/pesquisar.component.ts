import { Component, Input } from '@angular/core';

import { Clima } from './general_information';

import { WeatherService } from './get-information.service';


@Component({
  selector: 'pesquisar-cidade',
  templateUrl: './pesquisar.component.html',
  styleUrls: ['./pesquisar.component.scss']
})
export class PesquisarCidadeComponent {
  title = 'Clima agora';
  @Input() cidade : string;
  informacoesClima: Clima;
  pesquisando = false;

  constructor(private weatherService: WeatherService) {}

  onProcuraCidade(cidade: string) {
    this.limparDados(true);
    this.weatherService.getWeatherCity(cidade)
      .then(clima => {
        this.limparDados(false);
        this.informacoesClima = clima;
      });
  }

  limparDados(pesquisando: boolean) {
    if (pesquisando) {
      this.informacoesClima = null;
    }
    this.pesquisando = pesquisando;
  }
}
